package main

import (
	"fmt"
	"sync"
	"time"
)

type Agent interface {
	Start()
}

type PingAgent struct {
	ID string
	c  chan int
}

type PongAgent struct {
	ID string
	c  chan int
}

func SayPing(p PingAgent) {
	for range p.c {
		time.Sleep(time.Second)
		fmt.Println("Ping")
		p.c <- 1
	}
	wg.Done()
}

func SayPong(p PongAgent) {
	var n int = 0
	p.c <- 1
	for range p.c {
		if n < 5 {
			n++
			time.Sleep(time.Second)
			fmt.Println("Pong")
			p.c <- 1
		} else {
			close(p.c)
			wg.Done()
			return
		}
	}
}

var wg sync.WaitGroup

func main() {
	defer wg.Wait()
	var c chan int = make(chan int)

	wg.Add(2)
	go SayPing(PingAgent{"Ping Agent", c})
	go SayPong(PongAgent{"Pong Agent", c})
}
