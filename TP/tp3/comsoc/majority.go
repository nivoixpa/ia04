package comsoc

func MajoritySWF(p Profile) (count Count, err error) {
	count = make(Count)
	err = checkProfile(p)

	if err == nil {
		for _, alt := range p[0] {
			count[alt] = 0
		}
		for _, alts := range p {
			count[alts[0]]++
		}
	}
	return count, err
}

func MajoritySCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := MajoritySWF(p)

	if err == nil {
		bestAlts = maxCount(count)
	}
	return bestAlts, err
}
