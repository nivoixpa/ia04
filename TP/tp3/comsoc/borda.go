package comsoc

func BordaSWF(p Profile) (count Count, err error) {
	count = make(Count)
	err = checkProfile(p)

	if err == nil {
		for _, alts := range p {
			for i, alt := range alts {
				count[alt] += len(alts) - 1 - i
			}
		}
	}
	return count, err
}

func BordaSCF(p Profile) (bestAlts []Alternative, err error) {
	count, err := BordaSWF(p)

	if err == nil {
		bestAlts = maxCount(count)
	}
	return bestAlts, err
}
