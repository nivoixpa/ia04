package comsoc

import "errors"

func TieBreakFactory(altsOrder []Alternative) func([]Alternative) (Alternative, error) {
	return func(alts []Alternative) (bestAlt Alternative, err error) {
		for _, pref := range altsOrder {
			for _, alt := range alts {
				if pref == alt {
					return pref, nil
				}
			}
		}
		err = errors.New("Did not find corresponding match")
		return
	}
}
