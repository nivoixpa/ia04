package comsoc

import (
	"errors"
)

func ApprovalSWF(p Profile, thresholds []int) (count Count, err error) {
	count = make(Count)
	err = checkProfile(p)
	if len(thresholds) != len(p[0]) {
		err = errors.New("Tous les individus doivent donner un seuil")
	}

	if err == nil {
		for _, alt := range p[0] {
			count[alt] = 0
		}
		for i, alts := range p {
			for j := 0; j < thresholds[i]; j++ {
				count[alts[j]]++
			}
		}
	}
	return count, err
}

func ApprovalSCF(p Profile, thresholds []int) (bestAlts []Alternative, err error) {
	count, err := ApprovalSWF(p, thresholds)
	if err == nil {
		bestAlts = maxCount(count)
	}
	return bestAlts, err
}
