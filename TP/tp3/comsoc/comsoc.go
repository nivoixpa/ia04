package comsoc

import "errors"

type Alternative int

type Profile [][]Alternative

type Count map[Alternative]int

// Renvoie l'indice ou se trouve alt dans prefs
func rank(alt Alternative, prefs []Alternative) int {
	for i := 0; i < len(prefs); i++ {
		if prefs[i] == alt {
			return i
		}
	}
	return -1
}

// Renvoie vrai ssi alt1 est préférée à alt2
func isPref(alt1, alt2 Alternative, prefs []Alternative) bool {
	for i := 0; i < len(prefs); i++ {
		if prefs[i] == alt1 {
			return true
		} else if prefs[i] == alt2 {
			return false
		}
	}
	return false
}

// Renvoie les meilleures alternatives pour un décompte donné
func maxCount(count Count) (bestAlts []Alternative) {
	max := -1
	for k := range count {
		if count[k] > max {
			max = count[k]
			bestAlts = make([]Alternative, 1)
			bestAlts[0] = k
		} else if count[k] == max {
			bestAlts = append(bestAlts, k)
		}
	}
	return bestAlts
}

// Vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative n'apparaît qu'une seule fois par préférences
func checkProfile(prefs Profile) error {
	nbrAlts := len(prefs[0])
	for i := 0; i < len(prefs); i++ {
		if len(prefs[i]) != nbrAlts {
			return errors.New("Profil incomplet")
		}

		for j := 0; j < len(prefs[i])-1; j++ {
			for k := j + 1; k < len(prefs[i]); k++ {
				if prefs[i][j] == prefs[i][k] {
					return errors.New("Deux fois la même alternative")
				}
			}
		}
	}
	return nil
}

// Vérifie le profil donné, par ex. qu'ils sont tous complets et que chaque alternative de alts apparaît exactement une fois par préférences
func checkProfileAlternative(prefs Profile, alts []Alternative) error {
	for i := 0; i < len(prefs); i++ {
		if len(prefs[i]) != len(alts) {
			return errors.New("Profil incomplet")
		}

		for j := 0; j < len(alts); j++ {
			if !checkAlternativeInList(alts[j], prefs[i]) {
				return errors.New("Alternative manquante")
			}
		}
	}
	return nil
}

func checkAlternativeInList(alt Alternative, alts []Alternative) bool {
	for i := 0; i < len(alts); i++ {
		if alts[i] == alt {
			return true
		}
	}
	return false
}

func contains(alts []Alternative, alt Alternative) bool {
	for _, a := range alts {
		if a == alt {
			return true
		}
	}
	return false
}
