package main

import (
	"fmt"
	"tp3/comsoc"
)

func main() {
	var nbrVoters int = 5
	var nbrAlts int = 4
	profile := make(comsoc.Profile, nbrVoters)

	for i := 0; i < nbrVoters; i++ {
		profile[i] = make([]comsoc.Alternative, nbrAlts)
	}

	// profile[0] = []comsoc.Alternative{1, 2, 3}
	// profile[1] = []comsoc.Alternative{2, 3, 1}
	// profile[2] = []comsoc.Alternative{3, 1, 2}

	profile[0] = []comsoc.Alternative{3, 1, 2, 4}
	profile[1] = []comsoc.Alternative{3, 1, 2, 4}
	profile[2] = []comsoc.Alternative{3, 4, 2, 1}
	profile[3] = []comsoc.Alternative{4, 3, 2, 1}
	profile[4] = []comsoc.Alternative{1, 4, 3, 2}

	fmt.Println(comsoc.STV_SCF(profile))
}
