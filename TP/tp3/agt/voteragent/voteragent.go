package agt

type Alternative int
type AgentID int64

type AgentI interface {
	Equal(ag AgentI) bool
	DeepEqual(ag AgentI) bool
	Clone() AgentI
	String() string
	Prefers(a Alternative, b Alternative)
	Start()
}

type Agent struct {
	ID    AgentID
	Name  string
	Prefs []Alternative
}

func (a Agent) Equal(ag AgentI) bool

func (a Agent) DeepEqual(ag AgentI) bool

func (a Agent) Clone() AgentI

func (a Agent) Prefers(altA Alternative, altB Alternative)

func (a Agent) Start()
