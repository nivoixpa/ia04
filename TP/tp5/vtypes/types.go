package vtypes

import "time"

type Request struct {
	Choice []Alternative `json:"alts"`
}

type NewBallotRequest struct {
	Rule     string    `json:"rule"`
	Deadline time.Time `json:"deadline"`
	Voters   []string  `json:"voter-ids"`
	NAlts    int       `json:"#alts"`
}

type VoteRequest struct {
	AgentID string `json:"agent-id"`
	VoteID  string `json:"vote-id"`
	Prefs   []int  `json:"prefs"`
	Options []int  `json:"options"`
}

type ResultRequest struct {
	BallotID string `json:"ballot-id"`
}

type Response struct {
	Result int `json:"res"`
}

type NewBallotResponse struct {
	BallotID string `json:"ballot-id"`
}

type ResultResponse struct {
	Winner  int   `json:"winner"`
	Ranking []int `json:"ranking"`
}

type Alternative int

type Profile [][]Alternative

type Count map[Alternative]int
