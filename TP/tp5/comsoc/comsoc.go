package comsoc

import (
	"errors"
	"tp5/vtypes"
)

func rank(alt vtypes.Alternative, prefs []vtypes.Alternative) int {
	for i := 0; i < len(prefs); i++ {
		if prefs[i] == alt {
			return i
		}
	}
	return -1
}

func maxCount(count vtypes.Count) (bestAlts []vtypes.Alternative) {
	max := -1
	for k := range count {
		if count[k] > max {
			max = count[k]
			bestAlts = make([]vtypes.Alternative, 1)
			bestAlts[0] = k
		} else if count[k] == max {
			bestAlts = append(bestAlts, k)
		}
	}
	return bestAlts
}

func checkProfile(prefs vtypes.Profile) error {
	nbrAlts := len(prefs[0])
	for i := 1; i < len(prefs); i++ {
		if len(prefs[i]) != nbrAlts {
			return errors.New("Profil incomplet")
		}

		for j := 0; j < len(prefs[i]); j++ {
			for k := j + 1; k < len(prefs[i]); k++ {
				if prefs[i][j] == prefs[i][k] {
					return errors.New("Alternative dupliquée")
				}
			}
		}
	}
	return nil
}

func contains(alts []vtypes.Alternative, alt vtypes.Alternative) bool {
	for _, a := range alts {
		if a == alt {
			return true
		}
	}
	return false
}
